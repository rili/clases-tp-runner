Shader "Custom/DeformEffect" {
    Properties{
        _MainTex("Texture", 3D) = "white" {}
        _Distortion("Distortion", Range(0.0, 1.0)) = 0.1
        _Amount("Amount", Range(0.0, 1.0)) = 0.1
        _Speed("Speed", Range(0.0, 10.0)) = 1.0
        _Rotate("Rotate", Range(0.0, 360.0)) = 0.0
    }

        SubShader{
            Tags {"Queue" = "Transparent" "RenderType" = "Opaque"}
            LOD 100

            Pass {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct appdata {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                sampler2D _MainTex;
                float _Distortion;
                float _Amount;
                float _Speed;
                float _Rotate;

                v2f vert(appdata v) {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                float2 deformUV(float2 uv) {
                    float time = _Time.y * _Speed;
                    float angle = _Rotate * 3.14159 / 180.0;
                    float2 center = float2(0.5, 0.5);
                    float2 offset = uv - center;
                    float2 offsetRot = float2(offset.x * cos(angle) - offset.y * sin(angle), offset.x * sin(angle) + offset.y * cos(angle));
                    float dist = length(offsetRot);
                    float deformAmount = _Amount * sin(dist * 50.0 + time) * _Distortion;
                    float deformAmountRot = deformAmount * cos(angle);
                    float2 offsetFinal = offsetRot * (1.0 + deformAmountRot);
                    float2 uvFinal = offsetFinal + center;
                    return uvFinal;
                }

                fixed4 frag(v2f i) : SV_Target {
                    float2 uv = deformUV(i.uv);
                    fixed4 col = tex2D(_MainTex, uv);
                    return col;
                }
                ENDCG
            }
        }
            FallBack "Diffuse"
}


