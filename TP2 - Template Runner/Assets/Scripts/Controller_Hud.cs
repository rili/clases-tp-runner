﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public float distance = 0;
    public float record;


    private void Start()
    {
        gameOver = false;
        distance = 0;
        PlayerData playerData = SaveManager.LoadPlayerData();
        record = playerData.recordd;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            Controller_Player.isColliding = true;

            if (distance > record)
            {
                record = distance;
                SaveManager.SavePlayerData(this);
            }

            //SaveManager.SavePlayerData(this);
            Destroy(GameObject.FindGameObjectWithTag("Enemy"));

            gameOverText.gameObject.SetActive(true);
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("f1") + "\n Record: " + record.ToString("f1") + "\nR para reiniciar";
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("f1");
        }
    }
}
