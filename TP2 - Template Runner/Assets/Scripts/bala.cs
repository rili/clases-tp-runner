using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{
    public int velocidad;
    private Rigidbody rb;

    // Start is called before the first frame update

    void Start()
    {
        
    }

    private void OnEnable()
    {
        transform.Translate(velocidad * Vector3.right * Time.deltaTime);

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") == true)
        {
            Destroy(GameObject.FindGameObjectWithTag("Enemy"));
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Nafta") == true)
        {
            Destroy(GameObject.FindGameObjectWithTag("Nafta"));
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Limit") == true)
        {
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            OnEnable();
        }
    }
}
