using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barra : MonoBehaviour
{
    public Image Frente;
    public float TiempoContador;
    public float Tiempo;

    void Start()
    {

    }

    void Update()
    {
        if (Tiempo<=TiempoContador)
        {
            Tiempo = Tiempo - Time.deltaTime;
            Frente.fillAmount = Tiempo / TiempoContador;

            if (Frente.fillAmount == 0)
            {
                Controller_Hud.gameOver = true;
                Controller_Player.isColliding = true;
            }
        } 
    }

    public void RestarTiempo()
    {
        Tiempo -= 20;
    }

   public void SumarTiempo()
    {
        Tiempo += 10;
    }
}
