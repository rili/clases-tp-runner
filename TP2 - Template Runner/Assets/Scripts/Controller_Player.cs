﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    static public bool isColliding = false;
    Barra barra;
    GameObject Bal;
    BalasPool balasPool;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();

    }

    private void GetInput()
    {
        Jump();
        Duck();
        disparar();

    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void disparar()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Bal = BalasPool.Instance.pidebala();
            Bal.transform.position = transform.position + new Vector3(2, 0, 0);
        }
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //Destroy(this.gameObject);
            //Controller_Hud.gameOver = true;

            barra = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Barra>();
            barra.RestarTiempo();
            Destroy(GameObject.FindGameObjectWithTag("Enemy"));
        }

        if (collision.gameObject.tag == "Enemy")
        {
            isColliding = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }

        if (collision.gameObject.CompareTag("Nafta"))
        {
            barra = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Barra>();
            barra.SumarTiempo();
            Destroy(GameObject.FindGameObjectWithTag("Nafta"));
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            isColliding = false;
        }
    }


}
