using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalasPool : MonoBehaviour
{

    [SerializeField] private GameObject bala;
    [SerializeField] private int poolMax = 10;
    [SerializeField] private List<GameObject> listabalas;

    private static BalasPool instance;
    public static BalasPool Instance { get { return instance; } }

    // Start is called before the first frame update

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    void Start()
    {
        aņadirbalas(poolMax);
    }

    private void aņadirbalas(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject Bala = Instantiate(bala);
            Bala.SetActive(false);
            listabalas.Add(Bala);
            Bala.transform.parent = transform;

        }

    }

    public GameObject pidebala()
    {
        for(int i = 0; i < listabalas.Count; i++)
        {
            if(!listabalas[i].activeSelf)
            {
                listabalas[i].SetActive(true);
                return listabalas[i];
            }
        }
        aņadirbalas(1);
        listabalas[listabalas.Count - 1].SetActive(true);
        return listabalas[listabalas.Count - 1];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
